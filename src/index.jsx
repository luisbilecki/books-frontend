import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import store from './store/configureStore';

import AppRouter from './routers/AppRouter';

import './styles/main.scss';
import 'bootstrap/dist/css/bootstrap.min.css';

const main = (
  <Provider store={store}>
    <AppRouter />
  </Provider>
);

ReactDOM.render(main, document.getElementById('root'));

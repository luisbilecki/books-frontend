import * as types from './index';

export const register = user => {
  return {
    type: types.SIGN_UP,
    user,
  };
};

export const login = user => {
  return {
    type: types.SIGN_IN,
    user,
  };
};

export const logout = () => {
  return {
    type: types.LOGOUT,
  };
};

import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { createBrowserHistory as createHistory } from 'history';

import rootReducer from '../reducers';
import rootSaga from '../sagas';

export const history = createHistory();

const sagaMiddleware = createSagaMiddleware();
const middlewares = [sagaMiddleware];

// Logger middleware
if (process.env.NODE_ENV === 'development') {
  const { createLogger } = require('redux-logger');
  const logger = createLogger();

  middlewares.push(logger);
}

const store = createStore(rootReducer, applyMiddleware(...middlewares));

sagaMiddleware.run(rootSaga);

export default store;

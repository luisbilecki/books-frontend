import axios from './base';

export function signIn(email, password) {
  return axios
    .post('/auth/signin', {
      email,
      password,
    })
    .then(res => res.data);
}

export function signUp(name = '', email, password) {
  return axios
    .post('/auth/signup', {
      name,
      email,
      password,
    })
    .then(res => res.data);
}

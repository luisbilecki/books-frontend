import axios from 'axios';

import { getAccessToken } from '../helpers/utility';

import { notifyError } from '../components/Notification/Notification';

import store from '../store/configureStore';
import { logout } from '../actions/auth.actions';

const instance = axios.create({
  baseURL: process.env.API_URL || 'http://localhost:3000',
  headers: {
    'Content-Type': 'application/json',
  },
});

// Include JWT Token in Headers
instance.interceptors.request.use(async config => {
  const token = getAccessToken();

  if (token) {
    config.headers.Authorization = `Bearer ${token}`;
  }
  return config;
});

// Handling 401 response
instance.interceptors.response.use(
  response => {
    return response;
  },
  error => {
    if (error.response.status === 401 && error.config.url !== '/auth/signin') {
      notifyError('Your access has expired. Please sign in');
      store.dispatch(logout());
    }

    throw error;
  }
);

export default instance;

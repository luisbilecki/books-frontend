import { pick, omitBy, isNil } from 'lodash';
import axios from './base';

export function getBooks(page = 1) {
  return axios.get(`/books?page=${page}`).then(res => res.data);
}

export function getBook(isbn) {
  return axios.get(`/books/${isbn}`).then(res => res.data);
}

export function createBook(formData) {
  const data = omitBy(
    pick(formData, [
      'isbn',
      'title',
      'authors',
      'shortDescription',
      'publisher',
      'language',
      'pages',
    ]),
    isNil
  );

  return axios.post('/books', data).then(res => res.data);
}

export function editBook(isbn, editData = {}) {
  const data = omitBy(
    pick(editData, [
      'title',
      'authors',
      'shortDescription',
      'publisher',
      'language',
      'pages',
    ]),
    isNil
  );

  return axios.put(`/books/${isbn}`, data).then(res => res.data);
}

export function deleteBook(isbn) {
  return axios.delete(`/books/${isbn}`).then(res => res.data);
}

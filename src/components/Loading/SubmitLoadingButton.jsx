import React from 'react';
import PropTypes from 'prop-types';
import Button from 'react-bootstrap/Button';
import Spinner from 'react-bootstrap/Spinner';

const SubmitLoadingButton = ({ variant, isLoading, block, children }) => (
  <Button variant={variant} type="submit" block={block} disabled={isLoading}>
    {isLoading && (
      <Spinner
        className="mr-3"
        as="span"
        animation="border"
        size="sm"
        role="status"
        aria-hidden="true"
      />
    )}
    {children}
  </Button>
);

SubmitLoadingButton.propTypes = {
  variant: PropTypes.string.isRequired,
  isLoading: PropTypes.bool.isRequired,
  children: PropTypes.oneOfType([PropTypes.element, PropTypes.string])
    .isRequired,
  block: PropTypes.bool,
};

export default SubmitLoadingButton;

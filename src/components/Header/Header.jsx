import React from 'react';
import PropTypes from 'prop-types';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import Button from 'react-bootstrap/Button';

const Header = ({ title, baseUrl, links, activeLink, logout }) => {
  return (
    <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
      <Navbar.Brand href={process.env.PUBLIC_URL || '/dashboard'}>
        {title}
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="responsive-navbar-nav" />
      <Navbar.Collapse id="responsive-navbar-nav">
        <Nav
          className="ml-auto justify-content-end"
          activeKey={activeLink || ''}
        >
          {links &&
            links.map(link => {
              const { href, label } = link;

              return (
                <Nav.Item key={href}>
                  <Nav.Link href={`${baseUrl}${href}`}>{label}</Nav.Link>
                </Nav.Item>
              );
            })}
          {logout && (
            <Nav.Item>
              <Button variant="outline-primary" onClick={() => logout()}>
                Logout
              </Button>
            </Nav.Item>
          )}
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
};

Header.propTypes = {
  title: PropTypes.string.isRequired,
  baseUrl: PropTypes.string.isRequired,
  links: PropTypes.arrayOf(
    PropTypes.shape({
      href: PropTypes.string.isRequired,
      label: PropTypes.string.isRequired,
    })
  ),
  activeLink: PropTypes.string.isRequired,
  logout: PropTypes.func,
};

export default Header;

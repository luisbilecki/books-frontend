import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

// Configure toast container
toast.configure({
  autoClose: 5000,
  draggable: false,
  position: toast.POSITION.TOP_RIGHT,
  hideProgressBar: true,
});

export function notifyInfo(message) {
  toast.info(message);
}

export function notifySuccess(message) {
  toast.success(message);
}

export function notifyError(message) {
  toast.error(message);
}

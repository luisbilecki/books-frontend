import React from 'react';
import PropTypes from 'prop-types';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

const DisplayField = ({ label, value }) => (
  <Row>
    <Col>
      <p>
        <strong>{label}:</strong> {value}
      </p>
    </Col>
  </Row>
);

DisplayField.propTypes = {
  label: PropTypes.string.isRequired,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
};

export default DisplayField;

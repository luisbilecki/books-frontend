import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';

const HeaderData = ({ title, actionHref, actionLabel }) => {
  return (
    <Row className="justify-content-center">
      <Col sm={8}>
        <h3>{title}</h3>
      </Col>
      <Col sm={4} className="text-right">
        <Link to={actionHref}>
          <Button>{actionLabel}</Button>
        </Link>
      </Col>
    </Row>
  );
};

HeaderData.propTypes = {
  title: PropTypes.string.isRequired,
  actionHref: PropTypes.string.isRequired,
  actionLabel: PropTypes.string.isRequired,
};

export default HeaderData;

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Table from 'react-bootstrap/Table';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import Button from 'react-bootstrap/Button';
import SweetAlert from 'react-bootstrap-sweetalert';

class TableView extends Component {
  state = {
    showDeleteAlert: false,
    idToDelete: null,
  };

  showDeleteAlert = itemId => {
    this.setState({ showDeleteAlert: true, idToDelete: itemId });
  };

  hideDeleteAlert = () => {
    this.setState({ showDeleteAlert: false });
  };

  confirmDeleteAlert = () => {
    const { idToDelete } = this.state;
    const { deleteItem } = this.props;
    this.hideDeleteAlert();

    deleteItem(idToDelete);
  };

  render() {
    const { keyName, data, columnLabels, fields, resource } = this.props;
    const { showDeleteAlert } = this.state;

    return (
      <Row className="my-3">
        <Col>
          {showDeleteAlert && (
            <SweetAlert
              danger
              title="Are you sure?"
              showCancel
              confirmBtnText="Yes, delete it!"
              confirmBtnBsStyle="danger"
              onConfirm={this.confirmDeleteAlert}
              onCancel={this.hideDeleteAlert}
              btnSize="md"
            />
          )}
          <Table striped bordered responsive>
            <thead>
              <tr>
                {columnLabels.map((label, key) => (
                  <th key={key}>{label}</th>
                ))}
              </tr>
            </thead>
            <tbody>
              {data.map(book => {
                const tdsForData = fields.map(field => {
                  return <td key={field}>{book[field]}</td>;
                });

                return (
                  <tr key={book[keyName]}>
                    {tdsForData}
                    <td>
                      <ButtonGroup size="sm">
                        <Link to={`${resource}/show/${book[keyName]}`}>
                          <Button variant="outline-info">Show</Button>
                        </Link>
                        <Link to={`${resource}/edit/${book[keyName]}`}>
                          <Button variant="outline-primary">Edit</Button>
                        </Link>
                        <Button
                          variant="outline-danger"
                          onClick={() => this.showDeleteAlert(book[keyName])}
                        >
                          Delete
                        </Button>
                      </ButtonGroup>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </Table>
        </Col>
      </Row>
    );
  }
}

TableView.propTypes = {
  keyName: PropTypes.string.isRequired,
  data: PropTypes.array.isRequired,
  columnLabels: PropTypes.array.isRequired,
  fields: PropTypes.array.isRequired,
  resource: PropTypes.string.isRequired, // '/books'
  deleteItem: PropTypes.func.isRequired,
};

export default TableView;

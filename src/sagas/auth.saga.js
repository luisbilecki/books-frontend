import { put, call } from 'redux-saga/effects';

import * as types from '../actions';

import { signIn, signUp } from '../api/auth';
import { setAccessToken, clearAccessToken } from '../helpers/utility';

import { notifySuccess } from '../components/Notification/Notification';

export function* signInSaga(payload) {
  try {
    const { user } = payload;

    const response = yield call(signIn, user.email, user.password);

    yield setAccessToken(response.token);

    yield put({ type: types.SIGN_IN_SUCCESS, token: response.token });
  } catch (err) {
    yield put({ type: types.SIGN_UP_ERROR, response: err.response });
  }
}

export function* signUpSaga(payload) {
  try {
    const { user } = payload;

    const response = yield call(signUp, user.name, user.email, user.password);

    notifySuccess('Successfully signed up!');
    yield put({ type: types.SIGN_UP_SUCCESS, response });
  } catch (err) {
    yield put({ type: types.SIGN_UP_ERROR, response: err.response });
  }
}

export function* logoutSaga() {
  yield clearAccessToken();
}

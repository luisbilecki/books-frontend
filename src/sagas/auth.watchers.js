import { takeEvery, all } from 'redux-saga/effects';
import { signInSaga, signUpSaga, logoutSaga } from './auth.saga';

import * as types from '../actions';

export const watchSignInRequest = takeEvery(types.SIGN_IN, signInSaga);
export const watchSignUpRequest = takeEvery(types.SIGN_UP, signUpSaga);
export const watchLogout = takeEvery(types.LOGOUT, logoutSaga);

export default function* startWatchers() {
  yield all([watchSignInRequest, watchSignUpRequest, watchLogout]);
}

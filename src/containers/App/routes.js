import { lazy } from 'react';

export default [
  {
    component: lazy(() => import('../Dashboard/Dashboard')),
    exact: true,
    path: '',
  },
  {
    component: lazy(() => import('../Books/AllBooks')),
    exact: true,
    path: 'books',
  },
  {
    component: lazy(() => import('../Books/NewBook')),
    exact: true,
    path: 'books/new',
  },
  {
    component: lazy(() => import('../Books/ShowBook')),
    exact: true,
    path: 'books/show/:isbn',
  },
  {
    component: lazy(() => import('../Books/EditBook')),
    exact: true,
    path: 'books/edit/:isbn',
  },
];

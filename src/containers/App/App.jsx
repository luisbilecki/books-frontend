import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Route } from 'react-router-dom';
import { connect } from 'react-redux';
import Container from 'react-bootstrap/Container';

import Header from '../../components/Header/Header';
import { logout } from '../../actions/auth.actions';

import dashboardRoutes from './routes';

const headerLinks = [
  {
    href: '/books',
    label: 'List Books',
  },
  {
    href: '/books/new',
    label: 'New Book',
  },
];

class App extends Component {
  render() {
    const { match, doLogout, location } = this.props;
    const activeLink = location.pathname;

    return (
      <div>
        <Header
          title="Books"
          activeLink={activeLink}
          baseUrl="/dashboard"
          logout={doLogout}
          links={headerLinks}
        />
        <Container>
          {dashboardRoutes.map(route => {
            const { exact, path, ...otherProps } = route;

            return (
              <Route
                exact={exact}
                key={path}
                path={`${match.url}/${path}`}
                {...otherProps}
              />
            );
          })}
        </Container>
      </div>
    );
  }
}

App.propTypes = {
  doLogout: PropTypes.func.isRequired,
  match: PropTypes.object,
  location: PropTypes.object,
};

const mapDispatchToProps = dispatch => ({
  doLogout: () => dispatch(logout()),
});

export default connect(null, mapDispatchToProps)(App);

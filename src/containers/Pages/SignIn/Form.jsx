import React from 'react';
import PropTypes from 'prop-types';
import Form from 'react-bootstrap/Form';

import SubmitLoadingButton from '../../../components/Loading/SubmitLoadingButton';
import { isInvalidField } from '../../../helpers/utility';

const SignInForm = ({
  handleFormSubmit,
  handleInputChange,
  validationData,
  isLoading,
}) => {
  const isEmailInvalid = isInvalidField(validationData, 'email');
  const isPasswordInvalid = isInvalidField(validationData, 'password');

  return (
    <Form onSubmit={handleFormSubmit}>
      <Form.Group controlId="email">
        <Form.Label>Email address</Form.Label>
        <Form.Control
          type="email"
          placeholder="Enter email"
          onChange={handleInputChange}
          isInvalid={isEmailInvalid}
        />
        <Form.Control.Feedback type={isEmailInvalid ? 'invalid' : 'valid'}>
          Please enter a valid email
        </Form.Control.Feedback>
      </Form.Group>

      <Form.Group controlId="password">
        <Form.Label>Password</Form.Label>
        <Form.Control
          type="password"
          placeholder="Password"
          onChange={handleInputChange}
          isInvalid={isPasswordInvalid}
        />
        <Form.Control.Feedback type={isPasswordInvalid ? 'invalid' : 'valid'}>
          Please enter a password greater than 8 characters
        </Form.Control.Feedback>
      </Form.Group>
      <Form.Group>
        <SubmitLoadingButton
          variant="primary"
          type="submit"
          block={true}
          isLoading={isLoading}
        >
          Sign in
        </SubmitLoadingButton>
      </Form.Group>
    </Form>
  );
};

SignInForm.propTypes = {
  handleFormSubmit: PropTypes.func.isRequired,
  handleInputChange: PropTypes.func.isRequired,
  validationData: PropTypes.object.isRequired,
  isLoading: PropTypes.bool.isRequired,
};

export default SignInForm;

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import isEmail from 'validator/lib/isEmail';
import isLength from 'validator/lib/isLength';

import SignInForm from './Form';
import { notifyError } from '../../../components/Notification/Notification';

import { login } from '../../../actions/auth.actions';

class SignInPage extends Component {
  state = {
    email: '',
    password: '',
    validation: {},
    isPending: false,
  };

  componentDidUpdate(prevProps) {
    if (prevProps.response !== this.props.response) {
      this.setState({ isPending: false });
      this.handleLoginResponse();
    }
  }

  validateForm() {
    const { email, password } = this.state;

    const validateResult = {
      email: isEmail(email),
      password: isLength(password, { min: 8 }),
    };

    return validateResult;
  }

  handleFormSubmit = event => {
    const { doLogin } = this.props;

    event.preventDefault();

    const validation = this.validateForm();
    if (Object.values(validation).some(e => !e)) {
      this.setState({ validation });
      return;
    }

    const { email, password } = this.state;

    this.setState({ isPending: true });
    doLogin({ email, password });
  };

  handleInputChange = e => {
    const { target } = e;

    this.setState({ [target.id]: target.value });
  };

  handleLoginResponse() {
    const { response } = this.props;
    const status = response && response.status;

    if (status === 401 || status === 404) {
      notifyError('Invalid email or password');
    }
  }

  render() {
    const { validation, isPending } = this.state;

    return (
      <Container>
        <Row>
          <Col lg={6} md={6} className="p-4 mx-auto border">
            <h3 className="mb-4 mt-1 text-center">Sign In</h3>
            <SignInForm
              handleFormSubmit={this.handleFormSubmit}
              handleInputChange={this.handleInputChange}
              validationData={validation}
              isLoading={isPending}
            />
            <p className="mt-4 text-center">
              <Link to={'signup'}>Do not have account? Sign Up!</Link>
            </p>
          </Col>
        </Row>
      </Container>
    );
  }
}

SignInPage.propTypes = {
  doLogin: PropTypes.func.isRequired,
  response: PropTypes.object,
};

const mapStateToProps = response => ({
  response: response.auth && response.auth.response,
});

const mapDispatchToProps = dispatch => ({
  doLogin: userInfo => dispatch(login(userInfo)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SignInPage);

import React from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import { Link } from 'react-router-dom';

const NotFoundPage = () => (
  <Container>
    <Row className="justify-content-center mb-4">
      <h1>Ops!</h1>
    </Row>
    <Row className="justify-content-center mb-4">
      <h2>Page not found</h2>
    </Row>
    <Row className="justify-content-center mb-4">
      <Link to="/">Return to Home Page</Link>
    </Row>
  </Container>
);

export default NotFoundPage;

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import isEmail from 'validator/lib/isEmail';
import isLength from 'validator/lib/isLength';

import SignUpForm from './Form';
import { notifyError } from '../../../components/Notification/Notification';

import { register } from '../../../actions/auth.actions';
import { history } from '../../../store/configureStore';

class SignUpPage extends Component {
  state = {
    email: '',
    password: '',
    validation: {},
    isPending: false,
  };

  componentDidUpdate(prevProps) {
    if (prevProps.response !== this.props.response) {
      this.setState({ isPending: false });
      this.handleRegisterResponse();
    }
  }

  validateForm() {
    const { email, password } = this.state;

    const validateResult = {
      email: isEmail(email),
      password: isLength(password, { min: 8 }),
    };

    return validateResult;
  }

  handleFormSubmit = event => {
    const { doRegister } = this.props;

    event.preventDefault();

    const validation = this.validateForm();
    if (Object.values(validation).some(e => !e)) {
      this.setState({ validation });
      return;
    }

    const { name, email, password } = this.state;

    this.setState({ isPending: true });
    doRegister({ name, email, password });
  };

  handleInputChange = e => {
    const { target } = e;

    this.setState({ [target.id]: target.value });
  };

  handleRegisterResponse() {
    const { response } = this.props;
    const status = response && response.status;

    if (status === 400) {
      notifyError('Invalid email or password');
    } else if (response.success) {
      history.push('/');
    }
  }

  render() {
    const { validation, isPending } = this.state;

    return (
      <Container>
        <Row>
          <Col lg={6} md={6} className="p-4 mx-auto border">
            <h3 className="mb-4 mt-1 text-center">Sign Up</h3>
            <SignUpForm
              handleFormSubmit={this.handleFormSubmit}
              handleInputChange={this.handleInputChange}
              validationData={validation}
              isLoading={isPending}
            />
            <p className="mt-4 text-center">
              <Link to={'signin'}>Do you have an account? Sign in!</Link>
            </p>
          </Col>
        </Row>
      </Container>
    );
  }
}

SignUpPage.propTypes = {
  doRegister: PropTypes.func.isRequired,
  response: PropTypes.object,
};

const mapStateToProps = response => ({
  response: response.auth && response.auth.response,
});

const mapDispatchToProps = dispatch => ({
  doRegister: userInfo => dispatch(register(userInfo)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SignUpPage);

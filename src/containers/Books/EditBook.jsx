import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import Loading from '../../components/Loading/Loading';
import HeaderData from '../../components/DataView/HeaderData';

import { getBook, editBook } from '../../api/books';

import {
  notifyError,
  notifySuccess,
} from '../../components/Notification/Notification';

import BookForm from './BookForm';

import { history } from '../../store/configureStore';

class EditBook extends Component {
  state = {
    book: null,
    isPending: false,
  };

  componentDidMount() {
    const { match } = this.props;
    const { isbn } = match.params;

    getBook(isbn)
      .then(book =>
        this.setState({
          book: {
            ...book,
            shortDescription: book.short_description,
          },
        })
      )
      .catch(() => {
        history.push('/dashboard/books');
        notifyError('An error has occurred while fetching book data');
      });
  }

  updateBookRegister = editData => {
    const { book } = this.state;

    this.setState({ isPending: true });
    editBook(book.isbn, editData)
      .then(() => {
        notifySuccess('Book successfully updated!');

        this.setState({ isPending: false });
        history.push('/dashboard/books');
      })
      .catch(err => {
        const { response } = err;
        const { status } = response;

        this.setState({ isPending: false });

        if ([422, 400].includes(status)) {
          notifyError('Invalid data provided. Please verify and try again!');
          return;
        }

        notifyError('An error has occurred during book update');
      });
  };

  render() {
    const { book, isPending } = this.state;

    return !book ? (
      <Loading />
    ) : (
      <React.Fragment>
        <HeaderData
          title={`Edit Book - ${book.isbn}`}
          actionHref="/dashboard/books"
          actionLabel="Back"
        />
        <Row className="my-3">
          <Col>
            <BookForm
              createOrUpdateBook={this.updateBookRegister}
              isEdit={true}
              bookData={book}
              isPending={isPending}
            />
          </Col>
        </Row>
      </React.Fragment>
    );
  }
}

EditBook.propTypes = {
  match: PropTypes.object,
};

export default EditBook;

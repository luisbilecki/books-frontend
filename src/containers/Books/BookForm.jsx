import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';

import isLength from 'validator/lib/isLength';

import SubmitLoadingButton from '../../components/Loading/SubmitLoadingButton';
import { isInvalidField } from '../../helpers/utility';

class BookForm extends Component {
  state = {
    formData: {
      isbn: '',
      title: null,
      authors: null,
      shortDescription: null,
      publisher: null,
      language: null,
      pages: null,
    },
    validation: {},
  };

  validateForm = () => {
    const { formData } = this.state;

    const validateResult = {
      isbn: isLength(formData.isbn, { min: 0, max: 13 }),
      title: isLength(formData.title || '', { min: 0, max: 80 }),
      authors: isLength(formData.authors || '', { min: 0, max: 255 }),
      publisher: isLength(formData.publisher || '', { max: 60 }),
      language: isLength(formData.language || '', { max: 20 }),
    };

    return validateResult;
  };

  submitFormToParent = event => {
    const { formData } = this.state;
    event.preventDefault();

    const validation = this.validateForm();
    if (Object.values(validation).some(e => !e)) {
      this.setState({ validation });
      return;
    }

    this.props.createOrUpdateBook(formData);
  };

  handleInputChange = event => {
    const { target } = event;
    const { id, value } = target;

    this.setState(prevState => ({
      formData: { ...prevState.formData, [id]: value },
    }));
  };

  render() {
    const { validation } = this.state;
    const { isEdit, bookData, isPending } = this.props;

    return (
      <Form onSubmit={this.submitFormToParent}>
        <Row>
          <Col sm md={3}>
            <Form.Group controlId="isbn">
              <Form.Label>ISBN</Form.Label>
              <Form.Control
                placeholder="Enter isbn"
                onChange={this.handleInputChange}
                required
                defaultValue={(isEdit && bookData.isbn) || ''}
                isInvalid={isInvalidField(validation, 'isbn')}
                disabled={isEdit}
              />
              <Form.Control.Feedback
                type={isInvalidField(validation, 'isbn') ? 'invalid' : 'valid'}
              >
                Please enter a valid ISBN (max. length 13)
              </Form.Control.Feedback>
            </Form.Group>
          </Col>
          <Col sm md={9}>
            <Form.Group controlId="title">
              <Form.Label>Title</Form.Label>
              <Form.Control
                placeholder="Enter title"
                onChange={this.handleInputChange}
                required
                defaultValue={(isEdit && bookData.title) || ''}
                isInvalid={isInvalidField(validation, 'title')}
              />
              <Form.Control.Feedback
                type={isInvalidField(validation, 'title') ? 'invalid' : 'valid'}
              >
                Please enter a valid title (max. length 80)
              </Form.Control.Feedback>
            </Form.Group>
          </Col>
        </Row>
        <Row>
          <Col>
            <Form.Group controlId="authors">
              <Form.Label>Authors</Form.Label>
              <Form.Control
                placeholder="Enter authors"
                onChange={this.handleInputChange}
                required
                defaultValue={(isEdit && bookData.authors) || ''}
                isInvalid={isInvalidField(validation, 'authors')}
              />
              <Form.Control.Feedback
                type={
                  isInvalidField(validation, 'authors') ? 'invalid' : 'valid'
                }
              >
                Too long text for authors
              </Form.Control.Feedback>
            </Form.Group>
          </Col>
        </Row>
        <Row>
          <Col>
            <Form.Group controlId="shortDescription">
              <Form.Label>Short Description</Form.Label>
              <Form.Control
                as="textarea"
                placeholder="Enter short description"
                onChange={this.handleInputChange}
                defaultValue={(isEdit && bookData.shortDescription) || ''}
              />
            </Form.Group>
          </Col>
        </Row>
        <Row>
          <Col sm="12" md="6">
            <Form.Group controlId="publisher">
              <Form.Label>Publisher</Form.Label>
              <Form.Control
                placeholder="Enter publisher"
                onChange={this.handleInputChange}
                isInvalid={isInvalidField(validation, 'publisher')}
                defaultValue={(isEdit && bookData.publisher) || ''}
              />
              <Form.Control.Feedback
                type={
                  isInvalidField(validation, 'publisher') ? 'invalid' : 'valid'
                }
              >
                Please enter a valid publisher (max. length 60)
              </Form.Control.Feedback>
            </Form.Group>
          </Col>
          <Col sm md="4">
            <Form.Group controlId="language">
              <Form.Label>Language</Form.Label>
              <Form.Control
                placeholder="Enter language"
                onChange={this.handleInputChange}
                isInvalid={isInvalidField(validation, 'language')}
                defaultValue={(isEdit && bookData.language) || ''}
              />
              <Form.Control.Feedback
                type={
                  isInvalidField(validation, 'language') ? 'invalid' : 'valid'
                }
              >
                Please enter a valid language (max. length 20)
              </Form.Control.Feedback>
            </Form.Group>
          </Col>
          <Col sm md="2">
            <Form.Group controlId="pages">
              <Form.Label>Pages</Form.Label>
              <Form.Control
                type="number"
                placeholder="Enter pages"
                onChange={this.handleInputChange}
                defaultValue={isEdit && bookData.pages}
              />
            </Form.Group>
          </Col>
        </Row>
        <Form.Group>
          <SubmitLoadingButton
            variant="primary"
            type="submit"
            block={true}
            isLoading={isPending}
          >
            {isEdit ? 'Confirm' : 'Save'}
          </SubmitLoadingButton>
        </Form.Group>
      </Form>
    );
  }
}

BookForm.propTypes = {
  createOrUpdateBook: PropTypes.func.isRequired,
  isEdit: PropTypes.bool.isRequired,
  isPending: PropTypes.bool.isRequired,
  bookData: PropTypes.object,
};

export default BookForm;

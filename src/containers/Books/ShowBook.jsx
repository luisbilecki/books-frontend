import React, { Component } from 'react';
import PropTypes from 'prop-types';

import HeaderData from '../../components/DataView/HeaderData';
import DisplayField from '../../components/DataView/DisplayField';
import Loading from '../../components/Loading/Loading';
import { notifyError } from '../../components/Notification/Notification';

import { history } from '../../store/configureStore';

import { getBook } from '../../api/books';

class ShowBook extends Component {
  state = {
    book: null,
  };

  componentDidMount() {
    const { match } = this.props;
    const { isbn } = match.params;

    getBook(isbn)
      .then(book => this.setState({ book }))
      .catch(() => {
        history.push('/dashboard/books');
        notifyError('An error has occurred while fetching book data');
      });
  }

  renderBookData() {
    const { book } = this.state;

    const dataMap = [
      { label: 'ISBN', column: 'isbn' },
      { label: 'Title', column: 'title' },
      { label: 'Authors', column: 'authors' },
      { label: 'Short description', column: 'short_description', default: '' },
      { label: 'Publisher', column: 'publisher', default: '' },
      { label: 'Language', column: 'language', default: '' },
      { label: 'Pages', column: 'pages', default: '' },
    ];

    if (book) {
      return dataMap.map(data => (
        <DisplayField
          key={data.column}
          label={data.label}
          value={book[data.column] || data.default}
        />
      ));
    }

    return null;
  }

  render() {
    const { book } = this.state;

    return book ? (
      <div>
        <HeaderData
          title={`#${book && book.isbn} - Book Details`}
          actionHref="/dashboard/books"
          actionLabel="Back"
        />
        <div className="my-3">{this.renderBookData()}</div>
      </div>
    ) : (
      <Loading />
    );
  }
}

ShowBook.propTypes = {
  match: PropTypes.object.isRequired,
};

export default ShowBook;

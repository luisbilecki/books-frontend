import React, { Component } from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import HeaderData from '../../components/DataView/HeaderData';

import { createBook } from '../../api/books';

import {
  notifyError,
  notifySuccess,
} from '../../components/Notification/Notification';

import BookForm from './BookForm';

import { history } from '../../store/configureStore';

class NewBook extends Component {
  state = {
    isPending: false,
  };

  createBookRegister = formData => {
    this.setState({ isPending: true });

    createBook(formData)
      .then(() => {
        notifySuccess('Book successfully created!');

        this.setState({ isPending: false });

        history.push('/dashboard/books');
      })
      .catch(err => {
        const { response } = err;
        const { status } = response;

        this.setState({ isPending: false });

        if ([422, 400].includes(status)) {
          notifyError('Invalid data provided. Please verify and try again!');
          return;
        }

        notifyError('An error has occurred during book create');
      });
  };

  render() {
    const { isPending } = this.state;

    return (
      <React.Fragment>
        <HeaderData
          title="New Book"
          actionHref="/dashboard/books"
          actionLabel="Back"
        />
        <Row className="my-3">
          <Col>
            <BookForm
              createOrUpdateBook={this.createBookRegister}
              isEdit={false}
              bookData={{}}
              isPending={isPending}
            />
          </Col>
        </Row>
      </React.Fragment>
    );
  }
}

export default NewBook;

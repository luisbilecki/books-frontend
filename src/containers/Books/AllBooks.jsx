import React, { Component } from 'react';
import Pagination from 'react-js-pagination';

import HeaderData from '../../components/DataView/HeaderData';
import TableView from '../../components/DataView/Table';
import Loading from '../../components/Loading/Loading';
import {
  notifyError,
  notifySuccess,
} from '../../components/Notification/Notification';

import { getBooks, deleteBook } from '../../api/books';

class AllBooks extends Component {
  state = {
    books: null,
    currentPage: 1,
    itemsPerPage: 25,
    totalCount: 0,
  };

  componentDidMount() {
    this.updateBooksData();
  }

  updateBooksData(page) {
    const { currentPage } = this.state;

    getBooks(page || currentPage)
      .then(res => {
        this.setState({
          books: res.data,
          currentPage: res.current_page,
          totalCount: res.total_count,
          itemsPerPage: res.per_page,
        });
      })
      .catch(() => {
        // In Prod: Log using Sentry -> console.log(err);
        notifyError('An error has occurred during fetching all books');
      });
  }

  onPageChange = pageNumber => {
    this.updateBooksData(pageNumber);
  };

  deleteBook = isbn => {
    deleteBook(isbn)
      .then(() => {
        notifySuccess('Deleted successfully!');
        this.updateBooksData();
      })
      .catch(() => {
        // In Prod: Log using Sentry -> console.log(err);
        notifyError('An error has occurred during delete');
      });
  };

  render() {
    const { books, currentPage, itemsPerPage, totalCount } = this.state;

    return books ? (
      <div>
        <HeaderData
          title="List Books"
          actionHref="books/new"
          actionLabel="New"
        />
        <TableView
          keyName="isbn"
          data={books}
          columnLabels={['ISBN', 'Title', 'Authors', 'Actions']}
          fields={['isbn', 'title', 'authors']}
          resource={'books'}
          deleteItem={this.deleteBook}
        />
        <Pagination
          activePage={currentPage}
          itemsCountPerPage={itemsPerPage}
          totalItemsCount={totalCount}
          pageRangeDisplayed={10}
          onChange={this.onPageChange}
          itemClass="page-item"
          linkClass="page-link"
        />
      </div>
    ) : (
      <Loading />
    );
  }
}

export default AllBooks;

import { getAccessToken } from '../helpers/utility';

const defaultState = {
  token: getAccessToken(),
};

export default (state = defaultState, action) => {
  const { response, token } = action;

  switch (action.type) {
    case 'SIGN_UP_SUCCESS':
      return { response };
    case 'SIGN_UP_ERROR':
      return { response };
    case 'SIGN_IN_SUCCESS':
      return { token };
    case 'SIGN_IN_ERROR':
      return { response };
    case 'LOGOUT':
      return {};
    default:
      return state;
  }
};

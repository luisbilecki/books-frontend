import React, { Suspense, lazy } from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import PublicRoute from './PublicRoute';
import PrivateRoute from './PrivateRoute';
import NotFoundPage from '../containers/Pages/NotFoundPage';
import Loading from '../components/Loading/Loading';

import { history } from '../store/configureStore';

const SignInPage = lazy(() => import('../containers/Pages/SignIn/SignInPage'));
const SignUpPage = lazy(() => import('../containers/Pages/SignUp/SignUpPage'));
const DashboardPage = lazy(() => import('../containers/App/App'));

const AppRouter = () => (
  <Router history={history}>
    <div>
      <Switch>
        <Suspense fallback={<Loading />}>
          <PublicRoute path="/" component={SignInPage} exact />
          <PublicRoute path="/signin" component={SignInPage} />
          <PublicRoute path="/signup" component={SignUpPage} />
          <PrivateRoute path="/dashboard" component={DashboardPage} />
        </Suspense>
        <Route component={NotFoundPage} />
      </Switch>
    </div>
  </Router>
);

export default AppRouter;

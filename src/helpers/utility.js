const clearAccessToken = () => {
  localStorage.removeItem('accessToken');
};

const setAccessToken = token => {
  localStorage.setItem('accessToken', token);
};

const getAccessToken = () => localStorage.getItem('accessToken');

const isInvalidField = (validation, key) => {
  const result = validation && validation[key];

  return result === undefined ? null : !result;
};

export { clearAccessToken, setAccessToken, getAccessToken, isInvalidField };

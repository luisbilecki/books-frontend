# Books Frontend

![Netlify Status](https://api.netlify.com/api/v1/badges/f837ec4b-e6f4-46fa-9035-f2e7fbef46a9/deploy-status)

Frontend for Books CRUD using React, Redux, React-Router and Bootstrap.

https://upbeat-jones-e82da9.netlify.com/

---

## Requirements

Node.js

---

## How to run?

1. Install project dependencies using `yarn install`;

2. Run the project using `yarn start`;

3. Open in your browser `http://localhost:8080`;

4. To build project use `yarn build:prod`.

---

## Todo:

- Test actions and reducers using Jest;
- Separate webpack in two files: development and production.
